window.addEventListener('load', function () {
    var canvas = document.querySelector('#game'),
        ctx = canvas.getContext('2d'),
        shuffleButton = document.querySelector('#shuffleButton'),
        resetButton = document.querySelector('#resetButton');

    function Square(text, y, x) {
        return { text: text, x: x, y: y }
    }

    function drawSquares(squares, ctx) {
        var squareSize = 50;
        squares.forEach(function (square) {
            var x = (square.x - 1) * squareSize,
                y = (square.y - 1) * squareSize;

            if (square.text === '') {
                ctx.clearRect(x, y, squareSize, squareSize);
                return;
            }

            ctx.strokeStyle = '#ddd';
            ctx.fillStyle = '#fff';
            ctx.fillRect(x, y, squareSize, squareSize);
            ctx.strokeRect(x, y, squareSize, squareSize);
            ctx.fillStyle = '#000';
            ctx.font = '32px Serif';
            ctx.fillText(square.text, x + 10, y + 35);
        });
    }

    function canMove(empty, target) {
        return (empty.x === target.x || empty.y === target.y) && (empty.x !== target.x || empty.y !== target.y);
    }

    function moveSquares(squares, target) {
        var empty = squares.filter(function (square) {
            return square.text === '';
        })[0];
        if (!canMove(empty, target))
            return squares;

        if (empty.x === target.x) {
            var direction = Math.sign(empty.y - target.y);
            return squares.map(function (square) {
                if (square.x !== target.x)
                    return square;
                if (direction === -1 && (square.y > target.y || square.y < empty.y)
                    || direction === 1 && (square.y < target.y || square.y > empty.y))
                    return square;
                if (square === empty)
                    return Object.assign({}, square, { y: target.y });
                return Object.assign({}, square, { y: square.y + direction * 1 });
            });
        } else if (empty.y === target.y) {
            var direction = Math.sign(empty.x - target.x);
            return squares.map(function (square) {
                if (square.y !== target.y)
                    return square;
                if (direction === -1 && (square.x > target.x || square.x < empty.x)
                    || direction === 1 && (square.x < target.x || square.x > empty.x))
                    return square;
                if (square === empty)
                    return Object.assign({}, square, { x: target.x });
                return Object.assign({}, square, { x: square.x + direction * 1 });
            });
        }
    }

    function shuffleSquares(squares) {
        var x, y,
            shuffleCount = 1000,
            shuffled = squares.slice();
        while (shuffleCount > 0) {
            x = Math.floor(Math.random() * 4) + 1;
            y = Math.floor(Math.random() * 4) + 1;
            shuffled = moveSquares(shuffled, { x: x, y: y });
            shuffleCount--;
        }
        return shuffled;
    }

    var initSquares = [
        Square('1', 1, 1), Square('2', 1, 2), Square('3', 1, 3), Square('4', 1, 4),
        Square('5', 2, 1), Square('6', 2, 2), Square('7', 2, 3), Square('8', 2, 4),
        Square('9', 3, 1), Square('10', 3, 2), Square('11', 3, 3), Square('12', 3, 4),
        Square('13', 4, 1), Square('14', 4, 2), Square('15', 4, 3), Square('', 4, 4),
    ];
    var squares = initSquares;


    drawSquares(squares, ctx);

    canvas.addEventListener('click', function (evt) {
        var x = Math.ceil((evt.offsetX + 1) / 50),
            y = Math.ceil((evt.offsetY + 1) / 50);

        squares = moveSquares(squares, { x: x, y: y });
        drawSquares(squares, ctx);
    });

    shuffleButton.addEventListener('click', function (evt) {
        squares = shuffleSquares(initSquares);
        drawSquares(squares, ctx);
    });

    resetButton.addEventListener('click', function (evt) {
        squares = initSquares;
        drawSquares(squares, ctx);
    });
});
